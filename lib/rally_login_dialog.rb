require 'rubygems'
require 'tk'
require 'tkextlib/tile'

class RallyLoginDialog
  attr_reader :win
  
  def initialize(parent_window, config)
    def _login_to_rally()
      base_url       = @configuration.rally["base_url"]
      user_name      = @username_value.value
      password       = @password_value.value
      custom_headers = CustomHttpHeader.new
      custom_headers.name    = 'Task Writer'
      custom_headers.version = '0.1'
      custom_headers.vendor  = 'Rally Software'

      @slm = RallyRestAPI.new(:base_url => base_url,
        :username => user_name,
        :password => password,
        :http_headers => custom_headers)

      @username_value.value = ""
      @password_value.value = ""

      @win.grab_release
      @win.destroy
    end

    def _cancel
      @slm = nil
      @win.grab_release
      @win.destroy
    end

    @configuration = config
    @win = TkToplevel.new(parent_window, :title => "Login")
    @win.grab
    
    TkLabel.new(@win) { text "Username:" }.grid(:column => 0, :row => 0)
    TkLabel.new(@win) { text "Password:" }.grid(:column => 0, :row => 1)

    @username_value = TkVariable.new("")
    username_field = Tk::Tile::TEntry.new(@win).grid(:column => 1, :row => 0, :sticky => "ew")
    username_field.textvariable(@username_value)

    @password_value = TkVariable.new("")
    password_field = Tk::Tile::TEntry.new(@win).grid(:column => 1, :row => 1, :sticky => "ew")
    password_field.textvariable(@password_value)
    password_field.show('*')

    button_frame = TkFrame.new(@win).grid(:column => 1, :row => 2, :sticky => "ew")
    login_button = Tk::Tile::TButton.new(button_frame, :text => "Login").grid(:column => 0, :row => 0)
    cancel_button = Tk::Tile::TButton.new(button_frame, :text => "Cancel").grid(:column => 1, :row => 0)

    login_button.command proc { _login_to_rally }
    cancel_button.command proc { _cancel }

    @win.wait_window
  end

  def get_rally_context
    return @slm
  end

  def show
    if @win
      @win.show
    end
  end
end
