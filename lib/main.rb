require 'rubygems'
require 'rally_rest_api'
require 'tk'
require 'tkextlib/tile'
require 'yaml'
require 'lib/task_writer_configuration'
require 'rally_login_dialog'

class TaskWriter

  def initialize(configuration)
    @configuration = configuration

    ####
    # Frames
    ####
    @root = TkRoot.new { title "TaskWriter" }

    @custom_frame = TkFrame.new(@root) {
      pady 5
    }

    @add_task_frame = TkLabelFrame.new(@custom_frame) {
      text "Add A Custom Task"
      pady 10
    }

    @assign_frame = TkFrame.new(@add_task_frame) {
      pady 5
    }
    @listbox_frame = TkFrame.new(@custom_frame) {
      pady 5
    }

    ####
    # variables (model)
    ####
    @list_of_tasks          = [] # duplicate, contains tasks, not strings
    @default_task_combolist = [] # default task combobox objects - used in reset
    @story_points_value     = TkVariable.new(0)
    @story_value            = TkVariable.new("USXXXX")
    @story_desc_value       = TkVariable.new("")
    @hours_value            = TkVariable.new("")
    @descr_value            = TkVariable.new("")
    @list_of_tasks_value    = TkVariable.new([])
    @task_checkbox_var_list = Hash.new
    @standard_task_model    = Hash.new
    @configuration.tasks.each {
      |task|
      @standard_task_model[task.name] = TkVariable.new
    }

    ####
    # controls - needed by click events
    ####
    @add_developer_list = Tk::Tile::TCombobox.new(@assign_frame) #{ values developers }
    @task_list           = TkListbox.new(@listbox_frame)
    @task_list.listvariable(@list_of_tasks_value)

    #_login_to_rally()
  end

  ####
  # adds a task to the listbox and the internal list
  ####
  def add_task_to_list(task)
    tl = @list_of_tasks_value.list
    @list_of_tasks << task
    tl << task.to_s
    @list_of_tasks_value.list = tl
  end

  ####
  # creates the task in rally using 
  ####
  def create_task_in_rally(task)
    if task.assigned_to.nil?
      assigned_to = ''
    else
      assigned_to = task.assigned_to
    end

    if not @story.nil?
      rally_task = @slm.create(:task, :name => task.name,
        :estimate => task.task_estimate,
        :to_do => task.task_estimate,
        :owner => assigned_to,
        :work_product => @story,
        :project => @story.project,
        :state => "Defined",
        :task_index => 1)

      if rally_task.nil?
        p "something went wrong creating #{task.name} in rally for story #{@story.name}"
      end
    end

  end

  def _main_win()
    ################
    # button methods
    ################

    ####
    # lookup a story when the search button is clicked and update the rest
    # of the screen with the values from rally
    ####
    def search_button_click()
      @story = lookup_story(@story_value.value)
      unless @story.nil?
        @story_desc_value.value = @story.name
        @story_points_value.value = @story.plan_estimate.to_i
        point_value = @story_points_value.value.to_i
        @standard_task_model.each_key {
          |key|

          (@standard_task_model[key]).value = (@configuration.tasks.find { |t| t.name == key }).estimate_for(point_value)
        }
      end
    end

    ####
    # add a custom task to the list of tasks
    ####
    def add_task_click()
      task = Task.new(String.new(@descr_value.value))
      story_points = @story_points_value.value.to_i || 0
      hours = @hours_value.value || 0

      if hours.to_f == 0.0 || @descr_value.value.nil? || @descr_value.value.empty?
        # do nothing, not enough valid information to create a task
        # show alert box requesting description and hours
        Tk.messageBox(:type => "ok", :title => "Insufficient Information", :icon => "info", :message => "Tasks must have a description and hour estimate")
      else
        task.set_estimate_for(story_points, hours.to_f)
        task.assigned_to = @add_developer_list.get

        add_task_to_list(task)

        # clear fields
        @descr_value.value = ""
        @hours_value.value = ""
        @add_developer_list.set("")
      end
    end

    ####
    # clears form elements to zero state
    ####
    def reset_form()
      @list_of_tasks = []
      @list_of_tasks_value.list = []
      @task_checkbox_var_list.each_key { |key| (@task_checkbox_var_list[key]).value = 0 }
      @story_value.value = "USXXXX"
      @story_desc_value.value = ""
      @story_points_value.value = 0
      @standard_task_model.each_key {
        |key|
        (@standard_task_model[key]).value = 0
      }
      i = 0
      @default_task_combolist.each { 
        |combobox|

        # reset non-standard assignments to blank
        unless @configuration.tasks[i].nil?
          unless @configuration.tasks[i].assigned_to == combobox.get
            combobox.set(@configuration.tasks[i].assigned_to || "")
          end
        end

        i += 1
      }
      
      ## some bug somewhere causes the configuration to be modified 
      ## after a reset when one of the default tasks is added then
      ## the reset button is pressed. band-aid solution is to just
      ## reread the configuration
      @configuration = TaskWriterConfiguration.new("task_def.yml")
    end

    ####
    # call rally to add tasks in the list of tasks, reset the form on success
    ####
    def done_button_click()
      @list_of_tasks.each { |task| create_task_in_rally(task) }
      Tk.messageBox(:type => "ok", :title => "Tasks Added", :icon => "info", :message => "Tasks have been added to Rally, press the Reset button to continue")
    end

    ####
    # if one of the standard task checkboxes is clicked, this function will
    # add or remove that task from the list
    ####
    def toggle_task_in_list(task, estimate = 0, assigned_to = "")
      task.set_estimate_for(@story_points_value.value.to_i, estimate)
      task.assigned_to = assigned_to
      task_enabled_var = @task_checkbox_var_list[task.name]
      if task_enabled_var == 1
        add_task_to_list(task)
      elsif task_enabled_var == 0
        delete_task_from_list(task.to_s)
      end
    end

    ####
    # right click menu button to remove a task from the list, also toggle off
    # the standard task
    ####
    def delete_task_from_list(task)
      tl = @list_of_tasks_value.list
      found_task = @list_of_tasks.find { |t| s = t.to_s; s.eql?(task) }

      # remove task from list of strings (listbox)
      tl.delete_if { |item| item.to_s == task }

      if not found_task.nil?
        # turn off checkbox
        task_enabled_var = @task_checkbox_var_list[found_task.name]
        if task_enabled_var == 1
          task_enabled_var.value = 0
          @task_checkbox_var_list[found_task.name] = task_enabled_var
        end
      end

      # ensure task is removed from list of tasks
      @list_of_tasks.delete_if { |t| t.to_s == task }
      @list_of_tasks_value.list = tl
    end

    search_frame = TkFrame.new(@root)
    task_frame   = TkLabelFrame.new(@root) {
      text "Select Standard Tasks"
      pady 5
    }
    button_frame = TkFrame.new(@root) {
      pady 5
    }

    ## top of the screen, story information
    story_points = 0

    story_search_frame = TkFrame.new(search_frame) {
      pady 5
    }

    TkLabel.new(story_search_frame) { text "Story Name: " }.pack(:side => "left")

    story_value = @story_value

    story_name = Tk::Tile::TEntry.new(story_search_frame)
    story_name.textvariable(story_value)
    story_name.pack(:side => "left", :fill => "x", :expand => true, :padx => "3", :pady => "2")

    story_info_frame = TkFrame.new(search_frame) {
      pady 5
    }
    TkLabel.new(story_info_frame) { text "Story Description" }.grid(:column => 0, :row => 0, :columnspan => 6, :sticky => "ew")
    TkLabel.new(story_info_frame) { text "Story Points"}.grid(:column => 6, :row => 0, :columnspan => 2, :sticky => "e")

    story_desc_value = @story_desc_value
    story_desc = Tk::Tile::TLabel.new(story_info_frame) {
      anchor "w"
      justify "left"
      relief "sunken"
      wraplength "250px"
    }.textvariable(story_desc_value)
    story_desc.grid(:column => 0, :row => 1, :columnspan => 6, :sticky => "ew")

    story_points_value = @story_points_value
    story_points_label = Tk::Tile::TLabel.new(story_info_frame) {
      justify "left"
    }.textvariable(story_points_value)
    story_points_label.grid(:column => 5, :row => 1, :columnspan => 2, :sticky => "e")

    story_search_frame.pack(:fill => "x", :expand => true, :side => "top", :anchor => "w", :padx => "3")
    story_info_frame.pack(:fill => "both", :expand => true, :side => "bottom", :anchor => "w")

    TkGrid.columnconfigure(story_info_frame, 0, :weight => 1)
    TkGrid.columnconfigure(story_info_frame, 1, :weight => 0)

    search_button = Tk::Tile::TButton.new(story_search_frame, :text => "Search")
    search_button.pack(:side => "right", :ipadx => "3")
    search_button.command proc { search_button_click }

    search_frame.pack(:fill => "x", :anchor => "w", :expand => true)

    ## show standard tasks
    developers = @configuration.developers

    # list of tasks to show in the listbox at the bottom of the screen, will
    # add standard tasks to this list if they are checked
    @configuration.tasks.each {
      |task|
      individual_task_frame = TkFrame.new(@task_frame)

      task_enabled_var = TkVariable.new
      task_checkbox = Tk::Tile::TCheckButton.new(individual_task_frame) {
        text task.name
        variable task_enabled_var
      }.grid(:column => 0, :row => 0, :sticky => "w")

      @task_checkbox_var_list[task.name] = task_enabled_var

      TkLabel.new(individual_task_frame) { text "Hour Estimate: " }.grid(:column => 1, :row => 0, :sticky => "w")

      @standard_task_model[task.name].value = (task.estimate_for(story_points))
      hour_entry = Tk::Tile::TEntry.new(individual_task_frame).textvariable(@standard_task_model[task.name])
      hour_entry.width(5)
      hour_entry.grid(:column => 2, :row => 0, :sticky => "ew")

      TkLabel.new(individual_task_frame) { text "Assign to: " }.grid(:column => 3, :row => 0, :sticky => "e")

      combobox = Tk::Tile::TCombobox.new(individual_task_frame)
      combobox.values(developers)
      combobox.set(task.assigned_to)
      combobox.grid(:column => 4, :row => 0, :sticky => "e")
      @default_task_combolist << combobox

      TkGrid.columnconfigure(individual_task_frame, 0, :weight => 1)
      task_checkbox.command { toggle_task_in_list(task, @standard_task_model[task.name], combobox.get) }
      individual_task_frame.pack(:fill => "x", :anchor => "w", :expand => true)
    }

    task_frame.pack(:anchor => "w", :fill => "x", :expand => true)

    ## custom tasks
    TkLabel.new(@add_task_frame) { text "Description:" }.grid(:column => 0, :row => 0, :sticky => "e")
    descr = Tk::Tile::TEntry.new(@add_task_frame) { width "70" }.grid(:column => 1, :row => 0, :columnspan => 3)
    descr.textvariable(@descr_value)

    TkLabel.new(@add_task_frame) { text "Hour Estimate:" }.grid(:column => 0, :row => 1, :sticky => "w")
    hours = Tk::Tile::TEntry.new(@add_task_frame) { width "5" }.grid(:column => 1, :row => 1, :sticky => "w")
    hours.textvariable(@hours_value)

    TkLabel.new(@assign_frame) { text "Assign to:" }.pack(:side => "left", :anchor => "e")
    @add_developer_list.values(developers)
    @add_developer_list.pack(:side => "left", :anchor => "w", :expand => true, :fill => "x")

    @assign_frame.grid(:column => 3, :row => 1, :sticky => "e")

    ## listbox to show all tasks
    @task_list.grid(:column => 0, :row => 0, :sticky => "news")

    add_task_button = Tk::Tile::TButton.new(@add_task_frame) {
      text "Add Task"
    }
    add_task_button.command proc { add_task_click }
    add_task_button.grid(:column => 3, :row => 2, :sticky => "e")

    vscroll = TkScrollbar.new(@listbox_frame) { orient "vert"  }.grid(:column => 1, :row => 0, :sticky => "ns")
    hscroll = TkScrollbar.new(@listbox_frame) { orient "horiz" }.grid(:column => 0, :row => 1, :sticky => "ew")
    @task_list.yscrollbar(vscroll)
    @task_list.xscrollbar(hscroll)
    TkGrid.columnconfigure(@listbox_frame, 0, :weight => 1)
    TkGrid.rowconfigure(@listbox_frame, 0, :weight => 1)

    @add_task_frame.pack(:anchor => "w", :side => "top", :fill => "x", :expand => "true")
    @listbox_frame.pack(:anchor => "w", :side => "bottom", :fill => "both", :expand => "true")
    @custom_frame.pack(:anchor => "w", :fill => "both", :expand => true)

    list_of_tasks = @list_of_tasks_value

    menu = TkMenu.new
    menu.add('command', :label => 'Delete', :command => proc {
        delete_task_from_list(@task_list.get("active"))
    })

    @task_list.bind('Button-3') {
      |evt|
      menu.popup(evt.root_x, evt.root_y)
    }

    done_button = Tk::Tile::TButton.new(button_frame, :text => "Done")
    done_button.command proc { done_button_click }
    done_button.pack(:side => "right")

    reset_button = Tk::Tile::TButton.new(button_frame, :text => "Reset")
    reset_button.command proc { reset_form }
    reset_button.pack(:side => "right")

    button_frame.pack(:anchor => "e", :expand => true)
  end


  def _login_to_rally()
    workspace_name = @configuration.rally["workspace"]
    project_name   = @configuration.rally["project"]

    dialog = RallyLoginDialog.new(@root, @configuration)
    @slm = dialog.get_rally_context

    if @slm.nil?
      return false
    end

    @project = @slm.user.subscription.workspaces.find {
      |w|
      w.name == workspace_name && w.style != "UseCase"
    }.projects.find { |p| p.name == project_name }

    return true
  end

  def get_story_type(story_id)
    if story_id.upcase =~ /\AUS/
      type = :hierarchical_requirement
    elsif story_id.upcase =~ /\ADE/
      type = :defect
    end
    return type
  end
  
  def lookup_story(story_id)
    story_type = get_story_type(story_id)
    unless story_type.nil?
      query_result = @slm.find(story_type,
        :fetch => true,
        :project => @project,
        :project_scope_down => false,
        :project_scope_up => false) { equal :formatted_i_d, story_id.upcase }
    end
    story = nil
    if not query_result.nil?
      story = query_result.first
    else
      p "story #{story_id} was not found in: #{@workspace} - #{@project}"
    end
    story
  end

  def run
    _main_win
    success = _login_to_rally
    unless success
      Tk.exit
    end

    Tk.mainloop
  end
end
