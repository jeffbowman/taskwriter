require 'yaml'
require 'task'

class TaskWriterConfiguration
  attr_reader :tasks, :developers, :rally

  def initialize(filename)
    if @yml.nil?
      @yml = YAML::load_file(filename)
      @tasks = @yml["Tasks"].inject(Array.new) { |array, task| array.push(Task.new(task["task"]["name"], task["task"]["estimates"], task["task"]["assigned_to"]))}
      @developers = @yml["Developers"]
      @rally = @yml["Rally"]
    end
  end
end