class Task
  attr_reader :name, :task_estimate
  attr_accessor :assigned_to
  
  def initialize(name = "", estimates = [], assigned_to = nil)
    @name = name
    @estimates = {}
    @assigned_to = assigned_to
    @task_estimate = 0

    if estimates.empty?
      (0..13).each { |i| @estimates[i] = 0}
    else
      estimate_index = 0
      prev_index = 0
      @estimates[0] = 0
      estimates.each {
        |estimate|
        if estimate_index >= 2
          tmp = estimate_index
          estimate_index = estimate_index + prev_index
          prev_index = tmp
        else
          prev_index = estimate_index
          estimate_index += 1
        end
        @estimates[estimate_index] = estimate
      }
      
    end
  end

  def estimate_for(point_value)
    if point_value >= 0 and point_value <= 13
      @estimates[point_value] || 0
    else
      nil
    end
  end

  def set_estimate_for(point_value, estimate)
    @estimates[point_value] = estimate
    @task_estimate = estimate
  end

  def to_s
    "#{@name}, for #{@task_estimate}, #{if @assigned_to.nil? or @assigned_to.empty? then 'unassigned' else 'assigned to ' + @assigned_to end}"
  end
end
