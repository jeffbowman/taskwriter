$:.unshift File.join(File.dirname(__FILE__), "lib")

require 'main'
require 'task_writer_configuration'

TaskWriter.new(TaskWriterConfiguration.new("task_def.yml")).run
