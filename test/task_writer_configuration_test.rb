$:.unshift File.join(File.dirname(__FILE__),'..','lib')

require 'test/unit'
require 'task_writer_configuration'

class TaskReaderTest < Test::Unit::TestCase
  def setup
    @filename = File.join(File.dirname(__FILE__), "..", "task_def.yml")
    @configuration = TaskWriterConfiguration.new(@filename)
    @tasks = @configuration.tasks
    @developers = @configuration.developers
  end

  def test_reader_returns_correct_size
    expected_size = 9
    actual_size = @tasks.size
    assert_equal(expected_size, actual_size, "task sizes differ")
  end

  def test_reader_read_correct_estimate_values
    task = @tasks[0]
    assert_equal("Business Service", task.name, "first element task name is incorrect")
    assert_equal(2.5, task.estimate_for(5), "first element task estimate is incorrect")

    task = @tasks[4]
    assert_equal("Message Definition", task.name, "middle element task name is incorrect")
    assert_equal(0.3, task.estimate_for(1), "middle element task estimate is incorrect")

    task = @tasks[8]
    assert_equal("Javascript", task.name, "last element task name is incorrect")
    assert_equal(14, task.estimate_for(13), "last element task estimate is incorrect")
  end

  def test_assigned_to_is_correct_when_provided
    task = @tasks[8]
    assert_equal("tballard@hoovers.com", task.assigned_to, "assigned_to is incorrect")
  end

  def test_reader_returns_correct_number_of_developers
    expected_size = 5
    actual_size = @developers.size
    assert_equal(expected_size, actual_size, "developer list size is invalid")
  end

  def test_reader_reads_developers_correctly
    devs = [
        "developer0@example.com",
        "developer1@example.com",
        "developer2@example.com",
        "developer3@example.com"
    ]

    is_contained = @developers.each {|dev| devs.find_all { |obj| dev == obj } }.size == devs.size
    assert_equal(is_contained, true, "invalid read for developers")
  end
end
