$:.unshift File.join(File.dirname(__FILE__),'..','lib')

require 'test/unit'
require 'task'

class TaskTest < Test::Unit::TestCase
  def setup
    @task = Task.new("name", [0.25, 1, 2, 6, 13, 14], "developer0@example.com")
  end

  def test_empty_estimate_array_on_initialize_sets_all_zeros
    t = Task.new("name")
    (0..13).each {
      |i|
      assert_equal(t.estimate_for(i), 0)
    }
  end

  def test_should_return_a_correct_estimate_value_for_a_valid_point_value
    assert_equal(@task.estimate_for(1), 0.25)
    assert_equal(@task.estimate_for(2), 1)
    assert_equal(@task.estimate_for(3), 2)
    assert_equal(@task.estimate_for(5), 6)
    assert_equal(@task.estimate_for(8), 13)
    assert_equal(@task.estimate_for(13), 14)
  end

  def test_should_return_a_zero_estimate_for_non_valid_point_values
    assert_equal(@task.estimate_for(0), 0)
    assert_equal(@task.estimate_for(4), 0)
    assert_equal(@task.estimate_for(6), 0)
    assert_equal(@task.estimate_for(7), 0)
    assert_equal(@task.estimate_for(9), 0)
    assert_equal(@task.estimate_for(10), 0)
    assert_equal(@task.estimate_for(11), 0)
    assert_equal(@task.estimate_for(12), 0)
  end

  def test_should_return_nil_for_point_values_outside_the_range_1_to_13
    assert_nil(@task.estimate_for(14))
  end

  def test_set_estimate_for_point_value_should_set_estimate_for_correctly
    t = Task.new("descr")
    t.set_estimate_for(5, 2.5)
    t.set_estimate_for(13, 13)
    assert_equal(t.estimate_for(13), 13)
    assert_equal(t.estimate_for(0), 0)
    assert_equal(t.estimate_for(1), 0)
    assert_equal(t.estimate_for(2), 0)
    assert_equal(t.estimate_for(3), 0)
    assert_equal(t.estimate_for(5), 2.5)
    assert_equal(t.estimate_for(8), 0)
    assert_equal(t.task_estimate, 13)
  end

  def test_to_s_shoud_return_appropriate_string
    s = @task.to_s
    expected = "#{@task.name}, for #{@task.task_estimate}, assigned to #{@task.assigned_to}"
    assert_equal(s, expected)
  end

  def test_to_s_should_return_appropriate_string_when_no_assignment_is_made
    t = Task.new("task")
    t.set_estimate_for(2, 1)
    expected = "#{t.name}, for #{1}, unassigned"
    assert_equal(t.to_s, expected)
  end

  def test_to_s_should_return_appropriate_string_when_no_assignment_is_made_but_assigned_to_is_empty_string
    t = Task.new("task")
    t.set_estimate_for(2, 1)
    t.assigned_to = ""
    expected = "#{t.name}, for #{1}, unassigned"
    assert_equal(t.to_s, expected)
  end

end
